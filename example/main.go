package main

import (
	"context"
	"fmt"
	"net/http"

	"gitee.com/youkelike/gracefulapp"
	"gitee.com/youkelike/web"
)

func main() {
	s1 := web.NewHTTPServer()
	mockHandler1 := func(c *web.Context) { fmt.Println("mockHandler1") }
	s1.Use(http.MethodGet, "/", mockHandler1)

	s2 := web.NewHTTPServer()
	mockHandler2 := func(c *web.Context) { fmt.Println("mockHandler2") }
	s2.Use(http.MethodGet, "/", mockHandler2)

	app := gracefulapp.NewApp(gracefulapp.AppWithServer(s1, "server1", ":8080"), gracefulapp.AppWithServer(s2, "server2", ":8082"), gracefulapp.WithShutdownCallbacks(func(ctx context.Context) {
		fmt.Println("shudown callback")
	}))
	app.StartAndServe()
}
