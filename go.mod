module gitee.com/youkelike/gracefulapp

go 1.20

require gitee.com/youkelike/web v0.0.0-20231120015639-cc8fdb1877fb

require (
	github.com/google/uuid v1.4.0 // indirect
	github.com/hashicorp/golang-lru v1.0.2 // indirect
)
